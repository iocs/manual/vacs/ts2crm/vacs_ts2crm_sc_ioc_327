# IOC for Fast Data Acquisition of vacuum gauges at TS2

## Used modules

*   [ecmccfg](https://gitlab.esss.lu.se/e3/wrappers/ecat/e3-ecmccfg)
*   [ecmc](https://gitlab.esss.lu.se/e3/wrappers/ecat/e3-ecmc)


## Controlled devices

*   TS2-010CRM:Vac-ECATIO-10001
